package facci.validator.pruebaunitest


import junit.framework.TestCase
import java.util.regex.Pattern

class MainActivityTest : TestCase() {

    //Simulamos una base de datos mediante una clase
    class Users(
        var correo: String,
        var password: String,
    )

    val usuarios = Users(
        "Gustavo123@gmail.com", "Admin12342."
    )



    //Datos que se ingresan
    //Declaramos un correo nulo
    val correonullo = null

    //Declaramos una contraseña nula
    val passwordnull = null

    //Declaramos una correo que deseamos ingresar
    val correo = "Gustavo123@gmail.com"

    //Declaramos una contraseña que deseamos ingresar
    val contraseña= "Admin12342."

    //Declaramos la expresion regular para validar el correo
    val expresionRegular = Pattern.compile(
        "^" +
                "(?=.*[0-9])" +
                "(?=.*[a-z])" +
                "(?=.*[A-Z])" +
                "(?=.*[@#$%^&+=])" +
                "(?=\\S+$)" +
                ".{4,}" +
                "$"
    )

    //Testeo que el campo correo no sea nullo

    fun testValidateEmailNotNull(){
        assertNotNull(correonullo)
    }
    //Testeo que el campo contraseña no sea nullo
    fun testValidatePasswordNotNull() {
        assertNotNull(passwordnull)
    }
    //Testeo que el campo correo sea válido
    fun testValidateEmail() {
     assertTrue(expresionRegular.matcher(correo).matches())
    }

    //Validamos si existe el usuario
    fun testValidate() {
        assertEquals(usuarios.correo, correo)
        assertEquals(usuarios.password, contraseña)
    }




}