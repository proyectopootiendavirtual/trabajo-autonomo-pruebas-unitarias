package facci.validator.pruebaunitest


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater

import android.widget.Toast
import androidx.core.util.PatternsCompat
import facci.validator.pruebaunitest.databinding.ActivityMainBinding
import java.util.regex.Pattern


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnPress.setOnClickListener { validate() }
    }

     fun validate(){
        val result = arrayOf(validateEmail(), validatePassword())

        if (false in result){
            return
        }

        Toast.makeText(this, "Correo si existe", Toast.LENGTH_SHORT).show()
    }


     fun validateEmail() : Boolean {
        //Recuperamos el contenido del textInputLayout
        val email = binding.email.editText?.text.toString()

        return if (email.isEmpty()) {
            binding.email.error = "Correo no puede estar vacío"
            false
        }else if (!PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()) {
            binding.email.error = "Correo no válido"
            false
        }else{
            binding.email.error = null
            true
        }
    }

      fun validatePassword() : Boolean {

        val password = binding.password.editText?.text.toString()

        //Ubicamos las expresiones regulaes
        val passwordRegex = Pattern.compile(
            "^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +        //at least 1 lower case letter
                    "(?=.*[A-Z])" +        //at least 1 upper case letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$"
        )

        return if (password.isEmpty()){
            binding.password.error = "Contraseña no puede estar vacía"
            false
        }else if (!passwordRegex.matcher(password).matches()){
            binding.password.error = "Contraseña no válida"
            false
        }else{
            binding.password.error = null
            true
        }
    }





}


